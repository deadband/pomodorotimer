﻿using PomodoroTimer.BL.States;
using log4net;

namespace PomodoroTimer.BL
{
    internal class PomodoroContext : IPomodoroContext
    {
        private readonly ILog _log;
        private State _state;
        internal int WorkPeriodsBeforeLongBreak;
        internal IPomodoroConfiguration PomodoroConfiguration { get; }

        public PomodoroContext(IPomodoroConfiguration pomodoroConfiguration,
            ILog logger)
        {
            PomodoroConfiguration = pomodoroConfiguration;
            _log = logger;
        }

        public void Start()
        {
            WorkPeriodsBeforeLongBreak = PomodoroConfiguration.NumberOfCycles;
            _state = new WorkPeriod(this);
            _state.Enter();
            _log.Info("Timer Started");
            _log.Debug($"Pomodoro config: {PomodoroConfiguration}");
        }

        internal void ChangeState(State newState)
        {
            _state = newState;
            _state.Enter();
            _log.Info($"State Changed. New state {_state}");
        }

    }
}
