﻿using System.Timers;
using log4net;

namespace PomodoroTimer.BL.States
{
    internal class WorkPeriod : State
    {
        private Timer _timer;
        private readonly PomodoroContext _pomodoroContext;
        private readonly ILog _logger = LogManager.GetLogger(typeof(WorkPeriod));

        public WorkPeriod(PomodoroContext pomodoroContext)
        {
            _pomodoroContext = pomodoroContext;
        }

        internal override void Enter()
        {
            _timer = new Timer
            {
                AutoReset = false,
                Interval = _pomodoroContext.PomodoroConfiguration.WorkTimeSpan.TotalMilliseconds 
            };
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
            _pomodoroContext.WorkPeriodsBeforeLongBreak--;
            _logger.Debug($"{nameof(_pomodoroContext.WorkPeriodsBeforeLongBreak)} changed to" +
                          $" {_pomodoroContext.WorkPeriodsBeforeLongBreak}");
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Dispose();
            _pomodoroContext.ChangeState(
                _pomodoroContext.WorkPeriodsBeforeLongBreak == 0
                    ? (State)new LongBreak(_pomodoroContext)
                    : new ShortBreak(_pomodoroContext)
                );
        }
    }
}