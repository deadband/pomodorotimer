﻿using System.Timers;
using log4net;

namespace PomodoroTimer.BL.States
{
    internal class LongBreak :State
    {
        private Timer _timer;
        private readonly PomodoroContext _pomodoroContext;
        private readonly ILog _logger = LogManager.GetLogger(typeof(LongBreak));

        public LongBreak(PomodoroContext pomodoroContext)
        {
            _pomodoroContext = pomodoroContext;
        }

        internal override void Enter()
        {
            _timer = new Timer
            {
                AutoReset = false,
                Interval = _pomodoroContext.PomodoroConfiguration.LongBreakTimeSpan.TotalMilliseconds
            };
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
            _pomodoroContext.WorkPeriodsBeforeLongBreak = _pomodoroContext.PomodoroConfiguration.NumberOfCycles;
            _logger.Debug($"{nameof(_pomodoroContext.WorkPeriodsBeforeLongBreak)} changed to" +
                          $" {_pomodoroContext.WorkPeriodsBeforeLongBreak}");
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Dispose();
            _pomodoroContext.ChangeState(new WorkPeriod(_pomodoroContext));
        }
    }
}