﻿using System.Timers;

namespace PomodoroTimer.BL.States
{
    internal class ShortBreak : State
    {
        private Timer _timer;
        private readonly PomodoroContext _pomodoroContext;

        public ShortBreak(PomodoroContext pomodoroContext)
        {
            _pomodoroContext = pomodoroContext;
        }

        internal override void Enter()
        {
            _timer = new Timer
            {
                AutoReset = false,
                Interval = _pomodoroContext.PomodoroConfiguration.ShortBreakTimeSpan.TotalMilliseconds
            };
            _timer.Elapsed += OnTimerElapsed;
            _timer.Start();
        }

        private void OnTimerElapsed(object sender, ElapsedEventArgs e)
        {
            _timer.Dispose();
            _pomodoroContext.ChangeState(new WorkPeriod(_pomodoroContext));
        }
    }
}