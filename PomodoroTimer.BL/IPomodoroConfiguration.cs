﻿using System;

namespace PomodoroTimer.BL
{
    internal interface IPomodoroConfiguration
    {
        int NumberOfCycles { get; }
        TimeSpan WorkTimeSpan { get; }
        TimeSpan ShortBreakTimeSpan{ get; }
        TimeSpan LongBreakTimeSpan { get; }
    }
}