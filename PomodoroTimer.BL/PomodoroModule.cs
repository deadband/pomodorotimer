﻿using Ninject.Modules;

namespace PomodoroTimer.BL
{
    public class PomodoroModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IPomodoroConfiguration>().To<PomodoroConfiguration>();
            Bind<IPomodoroContext>().To<PomodoroContext>();
        }
    }
}
