﻿namespace PomodoroTimer.BL
{
    public interface IPomodoroContext
    {
        void Start();
    }
}