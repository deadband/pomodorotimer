﻿namespace PomodoroTimer.BL
{
    public abstract class State
    {
        internal abstract void Enter();
    }
}
