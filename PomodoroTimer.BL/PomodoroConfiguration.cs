﻿using System;
using System.Configuration;

namespace PomodoroTimer.BL
{
    internal class PomodoroConfiguration : IPomodoroConfiguration
    {
        public int NumberOfCycles => //get only implementation
            int.Parse(ConfigurationManager.AppSettings["numberOfCycles"]);

        public TimeSpan WorkTimeSpan => //get only implementation
             TimeSpan.Parse(ConfigurationManager.AppSettings["workTimeSpan"]);

        public TimeSpan ShortBreakTimeSpan => //get only implementation
             TimeSpan.Parse(ConfigurationManager.AppSettings["shortBreakTimeSpan"]);

        public TimeSpan LongBreakTimeSpan => //get only implementation
             TimeSpan.Parse(ConfigurationManager.AppSettings["longBreakTimeSpan"]);

        public override string ToString()
        {
            return $"NumberOfCycles: {NumberOfCycles}, WorkTimeSpan: {WorkTimeSpan}, " +
               $"ShortBreakTimeSpan: {ShortBreakTimeSpan}, LongBreakTimeSpan: {LongBreakTimeSpan}";
        }
    }
}