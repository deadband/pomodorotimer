﻿using log4net;
using Ninject.Modules;

namespace PomodoroTimer
{
    internal class UlModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILog>().ToMethod(ctx =>LogManager
                .GetLogger(ctx?.Request.Target?.Member.ReflectedType ?? typeof(Program)))
                .InTransientScope();
        }
    }
}
