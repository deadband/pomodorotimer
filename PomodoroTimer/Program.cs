using System;
using PomodoroTimer.BL;
using Ninject;
using log4net.Config;
using log4net;

namespace PomodoroTimer
{
    internal class Program
    {
        private static void Main()
        {
            XmlConfigurator.ConfigureAndWatch(new System.IO.FileInfo("Log4Net.xml"));
            var kernel = new StandardKernel(new PomodoroModule(), new UlModule());

            var context = kernel.Get<IPomodoroContext>();
            kernel.Get<ILog>().Info("Starting App");
            context.Start();
            Console.ReadKey();
        }
    }
}
